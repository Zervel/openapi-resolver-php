<?php

namespace OpenapiNextGeneration\OpenapiResolverPhp;

use OpenapiNextGeneration\OpenapiParserPhp\JsonParser;
use OpenapiNextGeneration\OpenapiParserPhp\YamlParser;

/**
 * Class to resolve references and place refered data where they should be
 */
class ReferenceResolver
{
    /**
     * Store of the specification data which is updated when an reference is resolved
     *
     * @var array
     */
    protected $specifications = [];
    /**
     * Flag that tracks if there are still unresolved references
     *
     * @var bool
     */
    protected $incomplete = true;

    /**
     * Resolve references in the specification until all references are resolved
     * Repeated until nothing flagged the specification as incomplete
     */
    public function resolveAllReferences(array $specification, string $currentFile): array
    {
        while ($this->incomplete) {
            $this->incomplete = false;
            $this->trackedRefs = [];
            $specification = $this->resolveAllReferencesRec($specification, $currentFile);
            $this->specifications[$this->getCleanFilename($currentFile)] = $specification;
        }

        return $specification;
    }

    /**
     * Resolve a single OpenApi $ref string
     */
    public function resolveRefString(string $refString, string $currentFile): ?Reference
    {
        $sharpPos = strpos($refString, '#');
        if ($sharpPos !== false) {
            $file = substr($refString, 0, $sharpPos);
            if (empty($file)) {
                $file = $currentFile;
            }
            $ref = substr($refString, $sharpPos + 2);
        } else {
            $file = $refString;
            $ref = null;
        }

        return $this->resolveReference($file, $ref);
    }

    /**
     * Resolves a reference by file-path and in-file reference
     */
    public function resolveReference(string $file, ?string $ref = null): ?Reference
    {
        $specification = $this->getFileSpecification($file);

        $reference = new Reference();
        $target = $specification;
        if (!empty($ref)) {
            $refSegments = explode('/', $ref);
            foreach ($refSegments as $refSegment) {
                if (!isset($target[$refSegment])) {
                    return null;
                }
                $target = $target[$refSegment];
            }
            $reference->setRefKey($refSegment);
        }
        $reference->setRefFile($file);
        $reference->setValue($target);

        return $reference;
    }

    /**
     * Internal recursive resolving method
     */
    protected function resolveAllReferencesRec(array $specification, string $currentFile): array
    {
        foreach ($specification as $key => $subDef) {
            if (count($specification) == 1 && $key === '$ref') {
                $targetRef = $this->resolveRefString($subDef, $currentFile);
                if (!$targetRef instanceof Reference) {
                    $this->incomplete = true;
                    continue;
                }

                $specification = array_replace_recursive($specification, $targetRef->getValue());
                foreach ($specification as $subKey => $subSubDef) {
                    if (is_array($subSubDef)) {
                        $specification[$subKey] = $this->resolveAllReferencesRec($subSubDef, $currentFile);
                    }
                }
            } elseif (is_array($specification[$key])) {
                $specification[$key] = $this->resolveAllReferencesRec($subDef, $currentFile);
            }
        }
        return $specification;
    }

    /**
     * Load specification from files that were not loaded before (or load from cache)
     */
    protected function getFileSpecification(string $file): array
    {
        $rawFilename = $file;
        $file = $this->getCleanFilename($file);

        if (!isset($this->specifications[$file])) {
            $fileContent = file_get_contents($file);

            $fileEnding = substr($file, strpos($file, '.') + 1);
            if (
                $fileEnding == 'yml'
                || $fileEnding == 'yaml'
            ) {
                $specification = YamlParser::parse($fileContent);
            } elseif ($fileEnding == 'json') {
                $specification = JsonParser::parse($fileContent);
            } else {
                $specification = YamlParser::parse($fileContent);
            }

            $this->specifications[$file] = $specification;
            $this->specifications[$file] = $this->resolveAllReferencesRec($specification, $rawFilename);
        }

        return $this->specifications[$file];
    }

    /**
     * Ensures that relative references works
     */
    protected function getCleanFilename(string $file): string
    {
        return realpath(ltrim($file, '/'));
    }
}