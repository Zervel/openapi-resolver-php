<?php

namespace OpenapiNextGeneration\OpenapiResolverPhp;

/**
 * Model that represents an openapi $ref
 */
class Reference
{
    protected $refKey;
    protected $refFile;
    protected $value;

    public function setRefKey(string $refKey): void
    {
        $this->refKey = $refKey;
    }

    public function setRefFile(string $refFile): void
    {
        $this->refFile = $refFile;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function getRefKey(): ?string
    {
        return $this->refKey;
    }

    public function getRefFile(): ?string
    {
        return $this->refFile;
    }

    public function getValue()
    {
        return $this->value;
    }
}